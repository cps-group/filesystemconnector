package io.github.cpsgroup.hypereton.plugin.connector.filesystem;


import io.github.cpsgroup.hypereton.plugin.connector.Connector;
import io.github.cpsgroup.hypereton.plugin.connector.SearchResponse;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Manuel Weidmann on 06.09.2014.
 */
@Component
public class FileSystemConnector implements Connector {

    private String targetFolder = "upload";

    @Override
    public boolean ping() {
        return new File(targetFolder).exists();
    }

    @Override
    public SearchResponse searchByString(String s) {

        SearchResponse response = new SearchResponse();

        // Get all files containing the search term 's';
        // as a regex: file names starting with arbitrary character sequences of arbitrary length,
        // followed by the search term, followed by an arbitrary sequence again
        ArrayList<File> list = getAllFiles(new File("upload"), "(.*)" + s + "(.*)");


        String[][] parsed = new String[list.size()][6];
        int index = 0;
        for (File file : list) {


            // check for a Makrolog-style file name
            String[] document = file.getName().substring(0, file.getName().length() - 4).split("_");
            if (document.length != 5) {
                parsed[index] = new String[]{file.getName(), file.getName(), "keine Angabe", file.getPath(), "1900",
                        "-1"};
            } else {
                parsed[index][0] = file.getName();
                parsed[index][1] = file.getName();
                String regionExtended;
                String regionShort = document[0];
                switch (regionShort) {
                    case "bd":
                        regionExtended = "Bund";
                        break;
                    case "bb":
                        regionExtended = "Brandenburg";
                        break;
                    case "be":
                        regionExtended = "Berlin";
                        break;
                    case "bw":
                        regionExtended = "Baden-Württemberg";
                        break;
                    case "by":
                        regionExtended = "Bayern";
                        break;
                    case "hb":
                        regionExtended = "Bremen";
                        break;
                    case "he":
                        regionExtended = "Hessen";
                        break;
                    case "hh":
                        regionExtended = "Hamburg";
                        break;
                    case "mv":
                        regionExtended = "Mecklenburg-Vorpommern";
                        break;
                    case "ni":
                        regionExtended = "Niedersachsen";
                        break;
                    case "nw":
                        regionExtended = "Nordrhein-Westfalen";
                        break;
                    case "rp":
                        regionExtended = "Rheinland-Pfalz";
                        break;
                    case "sh":
                        regionExtended = "Schleswig-Holstein";
                        break;
                    case "sl":
                        regionExtended = "Saarland";
                        break;
                    case "sn":
                        regionExtended = "Sachsen";
                        break;
                    case "st":
                        regionExtended = "Sachsen-Anhalt";
                        break;
                    case "th":
                        regionExtended = "Thüringen";
                        break;
                    default:
                        regionExtended = "keine Angabe";
                }

                parsed[index][2] = regionExtended;
                parsed[index][3] = file.getPath();
                parsed[index][4] = document[2];       //Date of the law
                try {
                    parsed[index][5] = String.valueOf(Integer.parseInt(document[4]) - Integer.parseInt(document[3]) +
                            1);

                } catch (NumberFormatException e) {
                    parsed[index][5] = "-1";
                }
//            word count
//            parsed[index][6] = ??}
            }

            index++;
        }

        response.setNumFound(index);
        response.setNumReturned(index);
        response.setLawArray(parsed);

        return response;
    }

    private ArrayList<File> getAllFiles(File file, String searchTerm) {
        ArrayList<File> list = new ArrayList<>();

        if (!file.isDirectory() && file.getName().matches(searchTerm)) list.add(file);
        else if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                for (File item : getAllFiles(f, searchTerm)) {
                    list.add(item);
                }
            }
        }

        return list;
    }

    @Override
    public void configure(Map<String, String> parameters) {
        if (parameters.containsKey("folder")) targetFolder = parameters.get("folder");
    }

    @Override
    public String getName() {
        return "FileSystem Connector";
    }

    @Override
    public String getDescription() {
        return "A simple file system connector." + System.lineSeparator() + "A target folder is required: Configure " +
                "it via the \"folder\" key and corresponding value";
    }
}
